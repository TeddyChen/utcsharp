﻿using System;
using NUnit.Framework;
using NFluent;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Spy.Exercise
{
	// TODO: Implement AlertSpy class to make the two test methods work

	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class WhenServerSendAlert
    {
		[Test()]
	    public void server_will_send_an_alert_when_a_command_execution_result_is_critical()
		{

			//        AlertSpy spy = new AlertSpy();
			//        Server server = new Server(spy);
			//        server.AddCommand(new DoorCommand(new OpenDoorStub()));
			//        server.Monitor();
			//        Check.That(spy.GetSentCount()).IsEqualTo(1);
			//
			//        server.AddCommand(new DoorCommand(new OpenDoorStub()));
			//        server.Monitor();
			//        Check.That(spy.GetSentCount()).IsEqualTo(3);
		}

		[Test()]
	    public void server_will_not_send_an_alert_when_a_command_execution_result_is_ok()
		{
			//        AlertSpy spy = new AlertSpy();
			//        Server server = new Server(spy);
			//        server.AddCommand(new DoorCommand(new ClosedDoorStub()));
			//        server.Monitor();
			//        Check.That(spy.GetSentCount()).IsEqualTo(0);
			//
			//        server.AddCommand(new DoorCommand(new ClosedDoorStub()));
			//        server.Monitor();
			//        Check.That(spy.GetSentCount()).IsEqualTo(0);
		}

		public class OpenDoorStub : IDoor
		{
			public String GetStatus()
    		{
		    	return "open";
	    	}
	    }

		public class ClosedDoorStub : IDoor
		{
			public String GetStatus()
		    {
			    return "not open";
		    }
		}
    }
}
