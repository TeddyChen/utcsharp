﻿using System;
using NUnit.Framework;
using NFluent;
using Moq;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Mock.Exercise
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class WhenServerSendAlert
    {
        [Test]
        public void server_will_send_an_alert_when_a_command_execution_result_is_critical()
        {
            //var spy = new Mock<IAlert>();
            //var mockCmd = new Mock<ICommand>();
            //mockCmd.Setup(cmd => cmd.Execute()).Returns(new Result(TW.Teddysoft.Monitor.Result.CRITICAL, "Door is open."));

            //Server server = new Server(spy.Object);
            //server.AddCommand(mockCmd.Object);
            //server.Monitor();
            //spy.Verify(alert => alert.SendAlert("Door is open."), Times.Once());

            //server.AddCommand(mockCmd.Object);
            //server.Monitor();
            //spy.Verify(alert => alert.SendAlert("Door is open."), Times.Exactly(3));
        }

        [Test]
        public void server_will_not_send_an_alert_when_a_command_execution_result_is_ok()
        {
			//var spy = new Mock<IAlert>();
			//var mockCmd = new Mock<ICommand>();
			//mockCmd.Setup(cmd => cmd.Execute()).Returns(new Result(TW.Teddysoft.Monitor.Result.OK, "Door is closed."));

			//Server server = new Server(spy.Object);
			//server.AddCommand(mockCmd.Object);
			//server.Monitor();
            //spy.Verify(alert => alert.SendAlert("Door is closed."), Times.Never());

			//server.AddCommand(mockCmd.Object);
			//server.Monitor();
			//spy.Verify(alert => alert.SendAlert("Door is closed."), Times.Never());
        }
    }
}
