﻿using System;
using NUnit.Framework;
using NFluent;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Dummy.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class ServerTest
    {
		[Test()]
	    public void command_size_are_two_when_add_two_commands_to_server()
		{
			Server server = new Server(null);
			Check.That(server.GetCommandSize()).IsEqualTo(0);

			server.AddCommand(new DummyCommand());
			server.AddCommand(new DummyCommand());

			Check.That(server.GetCommandSize()).IsEqualTo(2);
		}

		public class DummyCommand : ICommand
		{
			public Result Execute()
			{
				return null;
			}
	    }
    }
}
