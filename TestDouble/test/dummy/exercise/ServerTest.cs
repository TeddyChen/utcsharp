﻿using System;
using NUnit.Framework;
using NFluent;
using TW.Teddysoft.TestDouble.Monitor;

namespace TW.Teddysoft.TestDouble.Dummy.Exercise
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture()]
    public class ServerTest
    {
		[Test()]
	    public void command_size_are_two_when_add_two_commands_to_server()
		{
			Server server = new Server(null);
			Check.That(server.GetCommandSize()).IsEqualTo(0);

			/*
			  TODO: use Dummy object to test server.getComandSize()

				The following code will not work because
				the argument of the addCommand method cannot be null

				server.AddCommand(null);
				server.AddCommand(null);
				*/

			Check.That(server.GetCommandSize()).IsEqualTo(2);
		}

	}
}
