﻿using System;

namespace TW.Teddysoft.TestDouble.Monitor
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class Result
    {
        public static readonly int OK = 1;
		public static readonly int CRITICAL = 2;

		private int _status;
		private String _msg;

		public Result()
		{
			_status = OK;
			_msg = "OK";
		}

		public Result(int status, String msg)
		{
			_status = status;
			_msg = msg;
		}

		public int GetStatus()
		{
			return _status;
		}

		public String GetMessage()
		{
			return _msg;
		}
    }
}
