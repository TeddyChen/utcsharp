﻿using System;

namespace Tw.Teddysoft.BootCamp.Assertion
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class InvoiceBuilder
    {
		private double vatRate = 0;
		private int taxIncludedPrice = 0;
		private int taxExcludedPrice = 0;

        protected InvoiceBuilder(){
			vatRate = 0;
		    taxIncludedPrice = 0;
		    taxExcludedPrice = 0; 
        }

		public static InvoiceBuilder NewInstance()
		{
			return new InvoiceBuilder();
		}

		public InvoiceBuilder WithVatRate(double vatRate)
		{
			this.vatRate = vatRate;
			return this;
		}

		public InvoiceBuilder WithTaxIncludedPrice(int taxIncludedPrice)
		{
			this.taxIncludedPrice = taxIncludedPrice;
			taxExcludedPrice = 0;
			return this;
		}

		public InvoiceBuilder WithTaxExcludedPrice(int taxExcludedPrice)
		{
			this.taxExcludedPrice = taxExcludedPrice;
			taxIncludedPrice = 0;
			return this;
		}

		public Invoice Issue()
		{
			if (0 == taxIncludedPrice)
			{
				taxIncludedPrice = GetTaxIncludedPrice();
			}
			return new Invoice(taxIncludedPrice, vatRate,
					GetVAT(),
					GetTaxExcludedPrice());
		}

		private int GetTaxExcludedPrice()
		{
            return (int)Math.Round(taxIncludedPrice / (1 + vatRate), MidpointRounding.AwayFromZero);
		}

		private int GetTaxIncludedPrice()
		{
            return (int)Math.Round(taxExcludedPrice * (1 + vatRate), MidpointRounding.AwayFromZero);
		}

		private int GetVAT()
		{
            return (int)Math.Round(GetTaxExcludedPrice() * vatRate, MidpointRounding.AwayFromZero);
		}

	}

	//    private int getVAT(int taxIncludedPrice, double vatRate){
	//        return  taxIncludedPrice - getTaxExcludedPrice(taxIncludedPrice, vatRate);
	//    }
	
}
