﻿using System;
using NUnit.Framework;

namespace Tw.Teddysoft.BootCamp.Test.Param.Exercise
{
	/**
    *   Created by teddy at Teddysoft
    */
	// TODO: Use parameterized test cases to test the C# built-in Math.min method
	[TestFixture()]
    public class WhenGettingMinValue
    {
        //[TestCase(-1, -1, 999)]
        //[TestCase(-1000, -1000, -1)]
        //[TestCase(-5, -5, 0)]
        //[TestCase(100, 100, 101)]
        //[TestCase(0, 0, 200)]
        //[TestCase(-1, -1, 1)]
        //[TestCase(0, 0, 0)]
        //[TestCase(1, 1, 2)]
        public void Show_data_driven_test_with_TestCase_attribute(int expected, int first, int second)
        {
            Assert.AreEqual(expected, Math.Min(first, second));
        }

        [Test, TestCaseSource("data")]
        public void Show_data_driven_test_with_TestCaseSource(int expected, int first, int second)
        {
            Assert.AreEqual(expected, Math.Min(first, second));
        }

        //static object[] data =
        //{
        //    new object[] {-1, -1, 999 },
        //    new object[] {-1000, -1000, -4},
        //    new object[] {-5, -5, 0},
        //    new object[] {100, 100, 101},
        //    new object[] {0, 0, 200},
        //    new object[] {-1, -1, 1},
        //    new object[] {0, 0, 0},
        //    new object[] {1, 1, 2 }
        //};
    }
}