﻿using System;
using NUnit.Framework;


namespace BootCamp.test.param.ans
{
   
    [TestFixture()]
public class WhenGettingMinValue
{
    //   public WhenGettingMinValue()
    //   {
    //   }


    //@Parameterized.Parameters
    //public static List<Object[]> data()
    //{
    //	return Arrays.asList(new Object[][] {
    //		{-1, -1, 999},
    //		{-1000, -1000, -4},
    //		{-5, -5, 0},
    //		{100, 100, 101},
    //		{0, 0, 200},
    //		{-1, -1, 1},
    //		{0, 0, 0,},
    //		{1, 1, 2} });
    //}

    //private int expected;
    //private int first;
    //private int second;

    //public WhenGettingMinValue(int expected, int first, int second)
    //{
    //	this.expected = expected;
    //	this.first = first;
    //	this.second = second;
    //}

    //@Test
    //public void show_how_to_test_min_with_the_data_driven_way()
    //{
    //	assertEquals(expected, Math.min(first, second));
    //}


	    [TestCase(0, -1, -1, 999)]
	    [TestCase(1, -1000, -1000, -1)]
	    public void show_how_to_test_min_with_the_data_driven_way(int id, int expected, int first, int second)
	    {
	        Assert.AreEqual(expected, Math.Min(first, second));

	    }
    }
}
