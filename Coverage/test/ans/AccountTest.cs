﻿using System;
using NUnit.Framework;
using Tw.Teddysoft.Coverage.Domain;

namespace Tw.Teddysoft.Coverage.Test.Ans
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture]
    public class AccountTest
    {
        private static readonly double INIT_BALANCE = 20000;
		Account account;

        [SetUp]
	    public void SetUp()
		{
			account = new Account("teddy", "1234", INIT_BALANCE);
		}

        [TearDown]
	    public void TearDown()
		{
			// nothing to clenaup
		}

		[Test]
	    public void achieve_100_percent_statement_coverage_when_test_withdraw()
		{
            Assert.True(account.Withdraw(1000));
            Assert.AreEqual(19000, account.GetBalance());

            Assert.False(account.Withdraw(-1));
			Assert.AreEqual(19000, account.GetBalance());
		}

		[Test]
	    public void achieve_100_percent_branch_coverage_when_test_withdraw()
		{
			Assert.True(account.Withdraw(1000));
			Assert.False(account.Withdraw(-1));
			Assert.False(account.Withdraw(30000));
		}

		[Test]
	    public void consider_boundary_conditions_when_test_withdraw()
		{
			Assert.True(account.Withdraw(19999));
			Assert.True(account.Withdraw(1));
			Assert.False(account.Withdraw(1));
		}

		/*
			Is this test method name a good one?
		 */
		[Test]
	    public void testDeposit()
		{
			// A normal deposit amount
			Assert.True(account.Deposit(500));
			Assert.AreEqual(20500, account.GetBalance());

			// A negative deposit amount which is unacceptable
			Assert.False(account.Deposit(-300));
			Assert.AreEqual(20500, account.GetBalance());

			// A zero deposit amount which is unacceptable
			Assert.False(account.Deposit(0));
			Assert.AreEqual(20500, account.GetBalance());

		}

		[Test]
	    public void testId()
		{
			Assert.AreEqual("teddy", account.GetId());
		}
    }
}
