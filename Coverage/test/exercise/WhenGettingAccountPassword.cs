﻿using System;
using NUnit.Framework;
using Tw.Teddysoft.Coverage.Domain;

namespace Tw.Teddysoft.Coverage.Test.Exercise
{
	/**
    *   Created by teddy at Teddysoft
    */
    [TestFixture]
	public class WhenGettingAccountPassword
    {
        private static readonly String PASSWORD = "1234";
        Account account;

        [SetUp]
	    public void SetUp()
		{
			account = new Account("teddy", PASSWORD, 20000);
		}

		[Test]
	    public void the_xUnit3_way_to_test_exception()
		{
			try
			{
				account.GetPassword("trytogetpwd");
                Assert.Fail("Expected an AccessDeniedException to be thrown");
			}
			catch (AccessDeniedException e)
			{
                Assert.AreEqual("Invalid secret token.", e.Message);
			}
		}

		[Test]
		public void successfully_get_password_when_providing_the_correct_secret_token() 
		{
            Assert.AreEqual(PASSWORD, account.GetPassword("@!teddysoft*&_"));
		}

		[Test]
		public void test_exception_whit_Assert_Thorws()
		{
			var ex = Assert.Throws<Coverage.Domain.AccessDeniedException>(() => account.GetPassword("trytogetpwd"));
			Assert.AreEqual(ex.Message, "Invalid secret token.");

		}

		[Test]
		public void thrown_an_ArgumentException_when_providing_null_secret_token() 
		{
			//TODO: Implement this test method
		}
    }
}
