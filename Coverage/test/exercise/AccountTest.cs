﻿using System;
using NUnit.Framework;
using Tw.Teddysoft.Coverage.Domain;

namespace Tw.Teddysoft.Coverage.Test.Exercise
{
	/**
    *   Created by teddy at Teddysoft
    */
	[TestFixture]
    public class AccountTest
    {
		private static readonly double ACCEPTABLE_DELTA = 1.0;
		private static readonly double INIT_BALANCE = 20000;
		private static readonly string PASSWORD = "1234";
        Account account;

        [SetUp]
	    public void SetUp()
		{
			account = new Account("teddy", PASSWORD, INIT_BALANCE);
		}

		[TearDown]
	    public void TearDown()
		{
			// nothing to clenaup
		}


		[Test]
	    public void achieve_100_percent_statement_coverage_when_test_withdraw()
		{
			// TODO: Test the withdraw method to achieve 100% statement (line) coverage
		}

		[Test]
	    public void achieve_100_percent_branch_coverage_when_test_withdraw()
		{
			// TODO: Test the withdraw method to achieve 100% branch coverage
		}

		[Test]
	    public void consider_boundary_conditions_when_test_withdraw()
		{
			// TODO: Test withdraw method by considering boundary conditions
		}

		/*
			TODO: Think about that do we need to add this test case
			to achieve 100% line coverage for the Account class?
		 */
		//    [Test]
		//    public void testId(){
		//        assertEquals("teddy", account.getId());
		//    }


		/*
			TODO: Is this test method name a good one?
		 */
		[Test]
	    public void testDeposit()
		{
			// A normal deposit amount
            Assert.True(account.Deposit(500));
            Assert.AreEqual(20500, account.GetBalance());

			// A negative deposit amount which is unacceptable
            Assert.False(account.Deposit(-300));
			Assert.AreEqual(20500, account.GetBalance());

			// A zero deposit amount which is unacceptable
			Assert.False(account.Deposit(0));
			Assert.AreEqual(20500, account.GetBalance());
		}
    }
}
