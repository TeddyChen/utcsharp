﻿using System;

namespace Tw.Teddysoft.Coverage.Domain
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class Account
    {
        private static readonly String SECRET_TOKEN = "@!teddysoft*&_";
        private String id;
        private String password;
        private double balance;

        public Account(String id, String password, double balance)
        {
            this.id = id;
            this.password = password;
            this.balance = balance;
        }

        public String GetId()
        {
            return id;
        }

        public String GetPassword(String secretToken) 
        {
            if (null == secretToken)
                throw new ArgumentException("The Secret Token cannot be null.");

            if (SECRET_TOKEN.Equals(secretToken)){
                return password;
            }
            else {
                throw new AccessDeniedException("Invalid secret token.");
            }
        }

        public double GetBalance()
        {
            return balance;
        }

        public bool Withdraw(double amount)
        {
            if (amount > 0 && balance >= amount)
            {
                balance = balance - amount;
                return true;
            }
            return false;
        }

        public bool Deposit(double amount)
        {
            if (amount > 0)
            {
                balance = balance + amount;
                return true;
            }
            return false;
        }
    }
}