﻿using System;

namespace Tw.Teddysoft.Coverage.Domain
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class AccessDeniedException : Exception
    {
		public AccessDeniedException() : base()
		{
		}

		public AccessDeniedException(String message) : base (message)
		{
		}
    }
}
