﻿using System;
using NUnit.Framework;
using NFluent;

namespace Tw.Teddysoft.TDD.Scenario4.Step2
{
	/**
	* Created by teddy at Teddysoft
	*/
	public class InvoiceBuilderTest
    {
		[Test]
	    public void calc_tax_excluded_price_with_tax_included_price()
		{
			// expected = 94.28 -> 94
			Check.That(InvoiceBuilder.calcTaxExcludedPrice(99, 0.05)).IsEqualTo(94);

			// expected = 104.76 -> 105
			Check.That(InvoiceBuilder.calcTaxExcludedPrice(110, 0.05)).IsEqualTo(105);
		}

		[Test]
        public void calcVat_with_tax_included_price()
		{
			Check.That(InvoiceBuilder.calcVat(99, 0.05)).IsEqualTo(5);
			Check.That(InvoiceBuilder.calcVat(110, 0.05)).IsEqualTo(5);
		}

	}
}
