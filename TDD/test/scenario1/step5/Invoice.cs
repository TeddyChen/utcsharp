﻿using System;

namespace Tw.Teddysoft.TDD.Scenario1.Step5
{
    /**
     * Created by teddy at Teddysoft
     */    
    public class Invoice
    {
		private int taxIncludedPrice;
		private int vat;
		private int taxExcludedPrice;
		private double vatRate;

		public int getTaxIncludedPrice()
		{
			return taxIncludedPrice;
		}

		public int getVat()
		{
			return vat;
		}

		public int getTaxExcludedPrice()
		{
			return taxExcludedPrice;
		}

		public double getVatRate()
		{
			return vatRate;
		}
    }
}
