﻿using System;

namespace Tw.Teddysoft.TDD.Scenario2.Step3
{
    /**
     * Created by teddy at Teddysoft
     */
    public class InvoiceApp
    {
	    private int taxIncludedPrice;
	    private double vatRate;
	    
	    public void stop() {
	    }

	    public void start() {
	    }

	    public void collectingInput(int taxIncludedPrice, double vatRate) {
	        this.taxIncludedPrice = taxIncludedPrice;
	        this.vatRate = vatRate;
	    }

        public int getTaxIncludedPrice()
        {
            return taxIncludedPrice;
        }

        public double getVatRate()
        {
            return vatRate;
        }

        public void show(Invoice invoice)
        {
        }
    }
}
