﻿using System;
using NUnit.Framework;
using NFluent;

namespace Tw.Teddysoft.TDD.Scenario5.Step1
{
	/**
	* Created by teddy at Teddysoft
	*/
	public class InvoiceBuilderTest
    {
		[Test]
	    public void calc_tax_excluded_price_with_tax_included_price()
		{
			// expected = 94.28 -> 94
			Check.That(InvoiceBuilder.calcTaxExcludedPrice(99, 0.05)).IsEqualTo(94);

			// expected = 104.76 -> 105
			Check.That(InvoiceBuilder.calcTaxExcludedPrice(110, 0.05)).IsEqualTo(105);
		}

		[Test]
        public void calcVat_with_tax_included_price()
		{
			Check.That(InvoiceBuilder.calcVat(99, 0.05)).IsEqualTo(5);
			Check.That(InvoiceBuilder.calcVat(110, 0.05)).IsEqualTo(5);
		}


		[Test]
	    public void cal_tex_included_price_with_tax_excluded_price()
		{
			Check.That(InvoiceBuilder.calcTaxIncludedPrice(94, 0.05)).IsEqualTo(99);
			Check.That(InvoiceBuilder.calcTaxIncludedPrice(105, 0.05)).IsEqualTo(110);
		}

	}
}
