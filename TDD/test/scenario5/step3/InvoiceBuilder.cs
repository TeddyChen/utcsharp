﻿using System;

namespace Tw.Teddysoft.TDD.Scenario5.Step3
{
    /**
    *   Created by teddy at Teddysoft
    */
    public class InvoiceBuilder
    {
	    private int taxIncludedPrice;
	    private double vatRate;
	    private int taxExcludedPrice;

	    public static InvoiceBuilder newInstance() {
	        return new InvoiceBuilder();
	    }

	    public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice) {
	        this.taxIncludedPrice = taxIncludedPrice;
	        return this;
	    }

	    public InvoiceBuilder withVatRate(double vatRate) {
	        this.vatRate = vatRate;
	        return this;
	    }

	    public InvoiceBuilder withTaxExcludedPrice(int taxExcludedPrice) {
	        this.taxExcludedPrice = taxExcludedPrice;
	        return this;
	    }

	    public Invoice issue() {
	        if (isUseTaxExcludedPriceToCalculateInvoice())
	            taxIncludedPrice = InvoiceCalculator.calcTaxIncludedPrice(taxExcludedPrice, vatRate);

	        return new Invoice(taxIncludedPrice,
	                InvoiceCalculator.calcVat(taxIncludedPrice, vatRate),
	                InvoiceCalculator.calcTaxExcludedPrice(taxIncludedPrice, vatRate), vatRate);
	    }

	    private bool isUseTaxExcludedPriceToCalculateInvoice(){
	        return (0 == taxIncludedPrice && 0 != taxExcludedPrice);
	    }
    }
}
