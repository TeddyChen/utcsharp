﻿using System;

namespace Tw.Teddysoft.TDD.Scenario5.Step3
{
	/**
    *   Created by teddy at Teddysoft
    */
	public class InvoiceCalculator
    {
		public static int calcTaxIncludedPrice(int taxExcludedPrice, double vatRate)
		{
			return (int)Math.Round(taxExcludedPrice * (1 + vatRate), MidpointRounding.AwayFromZero);
		}

		public static int calcVat(int taxIncludedPrice, double vatRate)
		{
			return taxIncludedPrice - calcTaxExcludedPrice(taxIncludedPrice, vatRate);
		}

		public static int calcTaxExcludedPrice(int taxIncludedPrice, double vatRate)
		{
			return (int)Math.Round(taxIncludedPrice / (1 + vatRate), MidpointRounding.AwayFromZero);
		}
    }
}
