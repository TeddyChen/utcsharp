﻿using System;

namespace Tw.Teddysoft.TDD.Scenario5.Step2
{
    /**
     * Created by teddy at Teddysoft
     */
    public class InvoiceApp
    {
		private int taxIncludedPrice;
		private double vatRate;
		private int taxExcludedPrice;

		public void stop()
		{
		}

		public void start()
		{
		}

		public void collectingInput(int taxIncludedPrice, double vatRate, int taxExcludedPrice)
		{
			this.taxIncludedPrice = taxIncludedPrice;
			this.vatRate = vatRate;
			this.taxExcludedPrice = taxExcludedPrice;
		}

        public int getTaxIncludedPrice()
        {
            return taxIncludedPrice;
        }

		public int getTaxExcludedPrice()
		{
			return taxExcludedPrice;
		}

        public double getVatRate()
        {
            return vatRate;
        }

        public void show(Invoice invoice)
        {
        }
    }
}
