﻿using System;
using NUnit.Framework;
using NFluent;

namespace Tw.Teddysoft.TDD.Scenario5.Step2
{
    /**
     * Created by teddy at Teddysoft
     */
    public class InvoiceEndToEndTest
    {
            InvoiceApp app;
            Invoice invoice;
        
            [SetUp]
            public void SetUp(){
                app = new InvoiceApp();
            }
        
            [TearDown]
            public void TearDown(){
                if (null != app)
                    app.stop();
            }
        
            [Test]
            public void issuing_invoice_with_tax_included_price(){
		        app.start();
		        app.collectingInput(17000, 0.05, 0);

		        int taxIncludedPrice = app.getTaxIncludedPrice();
		        double vatRate = app.getVatRate();

		        invoice = InvoiceBuilder.newInstance().
		                withTaxIncludedPrice(taxIncludedPrice).
		                withVatRate(vatRate).
		                issue();

		        Check.That(invoice).IsNotNull();
		        Check.That(invoice.getTaxIncludedPrice()).IsEqualTo(17000);
		        Check.That(invoice.getVat()).IsEqualTo(810);
		        Check.That(invoice.getTaxExcludedPrice()).IsEqualTo(16190);
		        Check.That(invoice.getVatRate()).IsEqualTo(0.05);

		        app.show(invoice);
            }

            [Test]
            public void issuing_invoice_with_tax_included_price_round_example()
            {
		        app.start();
		        app.collectingInput(99, 0.05, 0);

		        int taxIncludedPrice = app.getTaxIncludedPrice();
		        double vatRate = app.getVatRate();

		        invoice = InvoiceBuilder.newInstance().
		                withTaxIncludedPrice(taxIncludedPrice).
		                withVatRate(vatRate).
		                issue();

		        Check.That(invoice).IsNotNull();
		        Check.That(invoice.getTaxIncludedPrice()).IsEqualTo(99);
		        Check.That(invoice.getVat()).IsEqualTo(5);
		        Check.That(invoice.getTaxExcludedPrice()).IsEqualTo(94);
		        Check.That(invoice.getVatRate()).IsEqualTo(0.05);

		        app.show(invoice);
            }

        [Test]
        public void should_be_tax_free_when_tax_included_price_is_10(){
            app.collectingInput(10, 0.05, 0);

            invoice = InvoiceBuilder.newInstance().
                withTaxIncludedPrice(app.getTaxIncludedPrice()).
                withVatRate(app.getVatRate()).
                issue();

            verifyInvoice(10, 0, 10, 0.05);
        }

		[Test]
	    public void could_issue_invoice_with_tax_excluded_price()
		{
	        app.collectingInput(0, 0.05, 16190);
	
	        invoice = InvoiceBuilder.newInstance().
	                withTaxExcludedPrice(app.getTaxExcludedPrice()).
	                withVatRate(app.getVatRate()).
	                issue();

			verifyInvoice(17000, 810, 16190, 0.05);
		}

		[Test]
	    public void vat_is_1_when_when_tax_excluded_price_is_10()
		{
			app.collectingInput(0, 0.05, 10);

			invoice = InvoiceBuilder.newInstance().
					withTaxExcludedPrice(app.getTaxExcludedPrice()).
					withVatRate(app.getVatRate()).
					issue();

			verifyInvoice(11, 1, 10, 0.05);
		}

        private void verifyInvoice( int taxIncludedPrice, int vat, int TaxExcluded, double vatRate){
	        Check.That(invoice).IsNotNull();
	        Check.That(invoice.getTaxIncludedPrice()).IsEqualTo(taxIncludedPrice);
	        Check.That(invoice.getVat()).IsEqualTo(vat);
	        Check.That(invoice.getTaxExcludedPrice()).IsEqualTo(TaxExcluded);
	        Check.That(invoice.getVatRate()).IsEqualTo(vatRate);
        }

    }
}
