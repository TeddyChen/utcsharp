﻿using System;

namespace Tw.Teddysoft.TDD.Scenario5.Step2
{
    /**
    *   Created by teddy at Teddysoft
    */
    public class InvoiceBuilder
    {
		private int taxIncludedPrice;
		private double vatRate;
		private int taxExcludedPrice;

		public static InvoiceBuilder newInstance()
		{
			return new InvoiceBuilder();
		}

		public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice)
		{
			this.taxIncludedPrice = taxIncludedPrice;
			return this;
		}

		public InvoiceBuilder withVatRate(double vatRate)
		{
			this.vatRate = vatRate;
			return this;
		}

		public Invoice issue()
		{
			if (isUseTaxExcludedPriceToCalculateInvoice())
				taxIncludedPrice = calcTaxIncludedPrice(taxExcludedPrice, vatRate);

			return new Invoice(taxIncludedPrice,
					calcVat(taxIncludedPrice, vatRate),
					calcTaxExcludedPrice(taxIncludedPrice, vatRate), vatRate);
		}

		public InvoiceBuilder withTaxExcludedPrice(int taxExcludedPrice)
		{
			this.taxExcludedPrice = taxExcludedPrice;
			return this;
		}

		public static int calcTaxIncludedPrice(int taxExcludedPrice, double vatRate)
		{
			return (int)Math.Round(taxExcludedPrice * (1 + vatRate), MidpointRounding.AwayFromZero);
		}

		public static int calcVat(int taxIncludedPrice, double vatRate)
		{
			return taxIncludedPrice - calcTaxExcludedPrice(taxIncludedPrice, vatRate);
		}

		public static int calcTaxExcludedPrice(int taxIncludedPrice, double vatRate)
		{
			return (int)Math.Round(taxIncludedPrice / (1 + vatRate), MidpointRounding.AwayFromZero);
		}

		private bool isUseTaxExcludedPriceToCalculateInvoice()
		{
			return (0 == taxIncludedPrice && 0 != taxExcludedPrice);
		}
    }
}
